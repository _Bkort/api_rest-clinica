package com.example.ajax.repository;

import org.springframework.stereotype.Repository;
import com.example.ajax.entidades.DetalleConsulta;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface IDetalleConsulta extends CrudRepository<DetalleConsulta, Integer> {

}