package com.example.ajax.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * DetalleConsulta
 */
@Entity
public class DetalleConsulta {

    // PARAMETROS
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idDC;
    @Column(name = "sintomas")
    private String sintomas;
    @ManyToOne(fetch = FetchType.EAGER)
    private Consulta consulta;

    // SETTER AND GETTER
    /**
     * @param idDC the idDC to set
     */
    public void setIdDC(Integer idDC) {
        this.idDC = idDC;
    }

    /**
     * @return the idDC
     */
    public Integer getIdDC() {
        return idDC;
    }

    /**
     * @param sintomas the sintomas to set
     */
    public void setSintomas(String sintomas) {
        this.sintomas = sintomas;
    }

    /**
     * @return the sintomas
     */
    public String getSintomas() {
        return sintomas;
    }

    /**
     * @param consulta the consulta to set
     */
    public void setConsulta(Consulta consulta) {
        this.consulta = consulta;
    }

    /**
     * @return the consulta
     */
    public Consulta getConsulta() {
        return consulta;
    }
}