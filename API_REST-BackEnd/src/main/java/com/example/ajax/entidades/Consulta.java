package com.example.ajax.entidades;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Consulta
 */
@Entity
public class Consulta {

    // PARAMETROS
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idConsulta;
    @Column(name = "fecha")
    private Date fecha;
    @Column(name = "diagnostico")
    private String diagnostico;
    @ManyToOne(fetch = FetchType.EAGER)
    private Doctor doctor;
    @ManyToOne(fetch = FetchType.EAGER)
    private Paciente paciente;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "consulta", cascade = CascadeType.ALL)
    private List<DetalleConsulta> detallesConsultas;

    // CONSTRUCTORES
    public Consulta() {

    }

    public Consulta(Integer idConsulta, Date fecha, String diagnostico) {
        this.idConsulta = idConsulta;
        this.fecha = fecha;
        this.diagnostico = diagnostico;
    }

    // SETTER AND GETTER
    /**
     * @return the idConsulta
     */
    public Integer getIdConsulta() {
        return idConsulta;
    }

    /**
     * @param idConsulta the idConsulta to set
     */
    public void setIdConsulta(Integer idConsulta) {
        this.idConsulta = idConsulta;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the diagnostico
     */
    public String getDiagnostico() {
        return diagnostico;
    }

    /**
     * @param diagnostico the diagnostico to set
     */
    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    /**
     * @return the doctor
     */
    public Doctor getDoctor() {
        return doctor;
    }

    /**
     * @param doctor the doctor to set
     */
    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    /**
     * @return the paciente
     */
    public Paciente getPaciente() {
        return paciente;
    }

    /**
     * @param paciente the paciente to set
     */
    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    /**
     * @param detallesConsultas the detallesConsultas to set
     */
    public void setDetallesConsultas(List<DetalleConsulta> detallesConsultas) {
        this.detallesConsultas = detallesConsultas;
    }

    /**
     * @return the detallesConsultas
     */
    public List<DetalleConsulta> getDetallesConsultas() {
        return detallesConsultas;
    }
}