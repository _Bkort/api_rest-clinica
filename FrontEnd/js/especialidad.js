let especialidad = { id: 0 };
function idEspecialidad(id) {
    especialidad.id = id;
}

$(document).ready(inicio);
function inicio() {
    cargarEspecialidad();
    $('#addEspecialidad').click(agregarEspecialidad);//add
    $('#deleteEspecialidad').click(function () {
        eliminarEspecialidad(especialidad.id);//delete
    });
    $('#updateEspecialidad').click(actualizarEspecialidad);//update
}

//funcion para listar las especialidades
function cargarEspecialidad() {
    $('#cEsp').html('');
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/especialidad/all",
        success: function (response) {
            response.forEach(i => {
                $('#cEsp').append(
                    '<tr>' +
                    '<td>' + i.id + '</td>' +
                    '<td>' + i.especialidad + '</td>' +
                    '<td>' +
                    "<button onclick='mostrarResgistro(" + i.id + ");' type='button' class='btn btn-outline-dark ml-4 mt-1' data-toggle='modal' data-target='#mUpdate'>Editar</button>" +
                    "<button onclick='idEspecialidad(" + i.id + ");' type='button' class='btn btn-outline-danger ml-4 mt-1' data-toggle='modal' data-target='#mDelete'>Eliminar</button>" +
                    '</td>' +
                    '</tr>');
            });
        },
        error: errorPeticion
    });
}

//funcion para un mensaje de error
function errorPeticion(response) {
    alert(response.Mensaje);
}

//funcion para agregar una nueva especialidad
function agregarEspecialidad() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/especialidad/save",
        data: { especialidad: $('#especialidad').val() },
        success: function (response) {
            alert(response.Mensaje);
            cargarEspecialidad();
        },
        error: errorPeticion
    });
}

//funcion para eliminar
function eliminarEspecialidad(id) {
    $.ajax({
        url: "http://localhost:8080/especialidad/delete/" + id,//lo mandamos par ael controlador
        method: "GET",
        success: function (response) {
            alert(response.Mensaje);
            cargarEspecialidad();
        },
        error: errorPeticion
    });
}

//funcioon para mostrar el registro dentro del modal
function mostrarResgistro(id) {
    $.ajax({
        url: "http://localhost:8080/especialidad/getEspecialidad/" + id,
        method: "GET",
        success: function (response) {
            $('#idE').val(response.id),
            $("#especialidad2").val(response.especialidad)
        },
        error: errorPeticion
    });
}

//funcion para actulizar
function actualizarEspecialidad() {
    var id = $('#idE').val();
    $.ajax({
        url: "http://localhost:8080/especialidad/update/" + id,
        method: "GET",
        data: {
            id: id,
            especialidad: $("#especialidad2").val()
        },
        success: function (response) {
            alert(response.Mensaje);
            cargarEspecialidad();
        },
        error: errorPeticion
    });
}