let paciente = { idPaciente: 0, nombre: "" };
let doctor = { id: 0, nombre: "" };

$(document).ready(inicio);

function inicio() {

    resetDetalles();
    cargarDetalle();
    cargarpaciente();
    cargardoctor();
    $('#agregarSintoma').click(agregarDetalle);//para guardar el sintoma
    $('body').on('click', '#guardar', guardarConsulta);//guardar la consulta

    /*PETIONES PARA LA VALIDACION DE LOS BOTONES
    QUE SE ENCARGARAN DE AGREGAR EL DOCTOR O PACIENTE
    DETRO DEL INPUT
    */
    $("body").on('click', '.agregarPaciente', function () {

        agregarPaciente($(this).parent().parent().children('td:eq(0)').text(), $(this).parent().parent().children('td:eq(1)').text());
    });
    $("body").on('click', '.agregarDoctor', function () {

        agregarDoctor($(this).parent().parent().children('td:eq(0)').text(), $(this).parent().parent().children('td:eq(1)').text());
    });
    /*********************************************/

    $()
}

//funcion para resetar los datos en cache
function resetDetalles() {
    $.ajax({
        url: "http://localhost:8080/consulta/resetDetalles",
        method: "GET"
    });
}

//funcion para error
function errorPeticion(response) {
    alert(response.Mensaje);
    /* Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'No funciono la peticion!',
    }) */
}
//funcion para agregar el sintoma
function agregarDetalle() {
    $.ajax({
        method: "POST",
        url: "http://localhost:8080/consulta/agregarDetalle",
        data: {
            sintomas: $('#sintoma').val()
        },
        success: function (response) {
            console.log(response.Mensaje);
            cargarDetalle();
            guardado2();
            resetDetalles();
            $('#sintoma').val('');
        },
        error: errorPeticion
    });
}
//funcion para mostrar el sintoma en la tabla
function cargarDetalle() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/consulta/detalle",
        success: function (response) {
            $('#dConsulta').html('');
            response.forEach(i => {
                $('#dConsulta').append(
                    '' +
                    '<tr>' +
                    '<td>' + i.sintomas + '</td>' +
                    '<td>' +
                    '<button class="bnt btn-danger" onclick="eliminarConsulta(' + i.id + ')">Eliminar</button>' +
                    '</td>' +
                    '</tr>'
                );
            });
        },
        error: errorPeticion
    });
}
//animacion para guardado
function guardado() {
    Swal.fire(
        'Guardado!',
        'El registro se guardo correctamente!',
        'success'
    )
}
function guardado2() {
    Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Guardado Correctamente',
        showConfirmButton: false,
        timer: 1500
    })
}
/****************************************/
//funcion para hacer cargar el regitro dentro del input
function agregarPaciente(idPaciente, nombre) {
    paciente.idPaciente = idPaciente;
    paciente.nombre = nombre;
    $('#paciente').val(nombre);
    console.log(paciente);
}
function agregarDoctor(id, nombre) {
    doctor.id = id;
    doctor.nombre = nombre;
    $('#doctor').val(nombre);
    console.log(doctor);
}
/**********************************/
//funcion para guardarla consulta
function guardarConsulta() {
    $.ajax({
        method: "POST",
        url: "http://localhost:8080/consulta/save",
        data: {
            fecha: $('#fecha').val(),
            diagnostico: $('#diagnostico').val(),
            id: doctor.id,
            idPaciente: paciente.idPaciente
        },
        success: function (response) {
            /* alert(response.Mensaje); */
            /* location.reload(); */
            guardado();
            resetDetalles();
        },
        error: errorPeticion
    });
}
//funcion para hacer cargar los paciente
function cargarpaciente() {
    //CARGANDO LOS  DE PACIENTES DESDE UN DATATABLE
    $('#paTabla').DataTable({
        "ajax": {
            "method": "GET",
            "url": "http://localhost:8080/paciente/allPacientes"
        },
        "columns": [
            {
                "data": "id",
                "whidth": "20%"
            },
            {
                "data": "nombre",
                "whidth": "20%"
            },
            {
                "data": "direccion",
                "whidth": "20%"
            },
            {
                "data": "accion",
                "whidth": "20%"
            }
        ],
        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "No hay registros",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Registros no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });
}
//funcion para hacer cargar los doctores
function cargardoctor() {
    $('#docTable').DataTable({
        "ajax": {
            "method": "GET",
            "url": "http://localhost:8080/doctor/allDoctores"
        },
        "columns": [
            {
                "data": "id",
                "whidth": "20%"
            },
            {
                "data": "nombre",
                "whidth": "20%"
            },
            {
                "data": "direccion",
                "whidth": "20%"
            },
            {
                "data": "especialidad",
                "whidth": "20%"
            },
            {
                "data": "accion",
                "whidth": "20%"
            }
        ],
        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "No hay registros",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Registros no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });
}

//funcion para remover el sintoma
function eliminarConsulta(id) {
    /* $.ajax({
        url: "http://localhost:8080/consulta/eliminarC/" + id,
        method: "Post",
        success: function (response) {
            alert(response.Mensaje);
            cargarDetalle();
        },
        error: errorPeticion
    }); */
    alert(id);
}