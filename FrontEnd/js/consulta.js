$(document).ready(inicio);
function inicio() {
    cargarConsulta();
    cargarPaciente();
    cargarDoctor();

    $('#addConsulta').click(agregarConsulta);//add
    $('#deleteConsulta').click(eliminarConsulta);//delete
    $('#updateConsulta').click(actualizarConsulta);
}
//funcion para agregar registro a la consulta
function cargarConsulta() {
    $('#cConsulta').html('');
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/consulta/all",
        success: function (response) {
            response.forEach(i => {
                $('#cConsulta').append('<tr>' +
                    '<td>' + i.idConsulta + '</td>' +
                    '<td>' + i.fecha + '</td>' +
                    '<td>' + i.paciente.nombre + '</td>' +
                    '<td>' + i.diagnostico + '</td>' +
                    '<td>' + i.doctor.nombre + '</td>' +
                    '<td>' +
                    "<button onclick='preModificar(" + i.idConsulta + ");' type='button' class='btn btn-outline-dark ml-4 mt-1' data-toggle='modal' data-target='#mUpdate'>Editar</button>" +
                    "<button onclick='preEliminar(" + i.idConsulta + ");' type='button' class='btn btn-outline-danger ml-4 mt-1' data-toggle='modal' data-target='#mDelete'>Eliminar</button>" +
                    '</td>' +
                    '</tr>'
                );
            });
        },
        error: errorPeticion
    });
}
//fucniones para caragar los nombres de pacientes y doctores
function cargarPaciente() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/consulta/allPaciente",
        success: function (response) {
            response.forEach(i => {
                $('#paciente').append(
                    '' + '<option value="' + i.idPaciente + '">' +
                    i.nombre +
                    '</option>' + '');
                $('#paciente2').append(
                    "" +
                    "<option value='" +
                    i.idPaciente + "'>" +
                    i.nombre +
                    "</option>"
                    + ""
                );
            });
        },
        error: errorPeticion
    });
}
function cargarDoctor() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/consulta/allDoctor",
        success: function (response) {
            response.forEach(i => {
                $('#doctor').append(
                    '' + '<option value="' + i.id + '">' +
                    i.nombre +
                    '</option>' + '');
                $('#doctor2').append(
                    "" +
                    "<option value='" +
                    i.id + "'>" +
                    i.nombre +
                    "</option>"
                    + ""
                );
            });
        },
        error: errorPeticion
    });
}
/*********************************************************/
//funcion para error
function errorPeticion(response) {
    alert(response.Mensaje);
}
//funcion para guardar el registro
function agregarConsulta() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/consulta/save",
        data: {
            fecha: $('#fecha').val(),
            paciente: $('#paciente').val(),
            diagnostico: $('#diagnostico').val(),
            doctor: $('#doctor').val()
        },
        success: function (response) {
            alert(response.Mensaje);
            cargarConsulta();
        },
        error: errorPeticion
    });
}
//funcion para Pre.eliminar
function preEliminar(idConsulta) {
    $('#idC').val(idConsulta);
}
//funcion para eliminar
function eliminarConsulta() {
    var id = $('#idC').val();
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/consulta/delete/" + id,
        success: function (response) {
            alert(response.Mensaje);
            cargarConsulta();
        },
        error: errorPeticion
    });
}
//funcion para cargar los registros dentro del modal de modificar
function preModificar(idConsulta) {
    /* alert(idConsulta); */
    $.ajax({
        url: "http://localhost:8080/consulta/getConsulta/" + idConsulta,
        method: "GET",
        success: function (response) {
            $('#idC').val(response.idConsulta),
                $('#fecha2').val(response.fecha),
                $('#paciente2').val(response.paciente.idPaciente),
                $('#diagnostico2').val(response.diagnostico),
                $('#doctor2').val(response.doctor.id)
        },
        error: errorPeticion
    });
}
//funcion para actualizar un registro
function actualizarConsulta() {
    var id = $('#idC').val();
    $.ajax({
        url: "http://localhost:8080/consulta/update/" + id,
        method: "GET",
        data: {
            id: id,
            fecha: $('#fecha2').val(),
            paciente: $('#paciente2').val(),
            diagnostico: $('#diagnostico2').val(),
            doctor: $('#doctor2').val()
        },
        success: function (response) {
            alert(response.Mensaje);
            cargarConsulta();
        },
        error: errorPeticion
    });
}

