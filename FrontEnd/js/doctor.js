$(document).ready(inicio);
function inicio() {
    cargarDoc();
    cEspecialidad();
    $('#addDoctor').click(addDoc);//add
    $('#deleteDoctor').click(eliminarDoc);//delete
    $('#updateDoctor').click(modificar);//update
}

//para cargar el listado de doctores
function cargarDoc(response) {
    //resetear la tabla
    $('#cDoctor').html('');
    //peticion ajax para listar los registros
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/doctor/all",
        success: function (response) {
            response.forEach(i => {
                $('#cDoctor').append(
                    '<tr>' +
                    '<td>' + i.id + '</td>' +
                    '<td>' + i.nombre + '</td>' +
                    '<td>' + i.direccion + '</td>' +
                    '<td>' + i.especialidad.especialidad + '</td>' +
                    '<td>' +
                    "<button onclick='preModificar(" + i.id + ");' type='button' class='btn btn-outline-dark ml-4 mt-1' data-toggle='modal' data-target='#mUpdate'>Editar</button>" +
                    "<button onclick='preEliminar(" + i.id + ");' type='button' class='btn btn-outline-danger ml-4 mt-1' data-toggle='modal' data-target='#mDelete'>Eliminar</button>" +
                    '</td>' +
                    '</tr>'
                );
            });
        },
        error: error
    });
}

//funcion para cargar el listado de las especialidades
function cEspecialidad() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/doctor/allEspecialidad",
        success: function (response) {
            response.forEach(i => {
                //para agregar seleccionar una especialidad
                $('#especialidad').append(
                    '' + '<option value="' + i.id + '">' +
                    i.especialidad +
                    '</option>' + '');
                $('#especialidad2').append(
                    "" +
                    "<option value='" +
                    i.id + "'>" +
                    i.especialidad +
                    "</option>"
                    + ""
                );
            });
        },
        error: error
    });
}

//funcion para error
function error(response) {
    alert('No funciono la peticion' + response.Mensaje);
    console.log('Error al realizar la peticion' + response.Error);
}

//funcion para guardar el registro ingresado en el formulario
function addDoc() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/doctor/save",
        data: {
            nombre: $('#nombre').val(),
            direccion: $('#direccion').val(),
            idEs: $('#especialidad').val()
        },
        success: function (response) {
            /* alert(response.Mensaje); */
            guardado();
            cargarDoc();
            
        },
        error: error
    });
}

//funcion para pre eliminar
function preEliminar(id) {
    $('#idDoctor').val(id);
}

//funcion para eliminar
function eliminarDoc() {

    var id = $('#idDoctor').val();

    $.ajax({
        method: "GET",
        url: "http://localhost:8080/doctor/delete/" + id,
        success: function (response) {
            alert(response.Mensaje);
            cargarDoc();
        },
        /* error: error */
    });
}

//funcion preModificar
function preModificar(id) {
    // alert(id);
    $.ajax({
        url: "http://localhost:8080/doctor/getDoctor/" + id,
        method: "GET",
        success: function (response) {
            $("#id").val(response.id);
            $("#nombre2").val(response.nombre);
            $("#direccion2").val(response.direccion);
            $("#especialidad2").val(response.especialidad.id);
        },
        error: error
    });
}

//funcion para modificar
function modificar() {
    var id = $('#id').val();
    $.ajax({
        url: "http://localhost:8080/doctor/update/" + id,
        method: "GET",
        data: {
            id: id,
            nombre: $("#nombre2").val(),
            direccion: $("#direccion2").val(),
            idEs: $("#especialidad2").val()
        },
        success: function (response) {
            alert(response.Mensaje);
            cargarDoc();
        },
        error: error
    });
}

function guardado() {
    Swal.fire(
        'Guardado',
        'El registro se guardo!',
        'success'
    )
}