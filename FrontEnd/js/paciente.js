let paciente = { id: 0 };
function idPaciente(id) {
    paciente.id = id;
}
$(document).ready(inicio);
function inicio() {
    cargarPaciente();

    $('#addPaciente').click(agregarPaciente);//add
    $('#deletePaciente').click(function () {
        eliminarPaciente(paciente.id);
    });
    $('#updatePaciente').click(actualizarPaciente);

    $('.agregarPaciente').click();

    $('#tablaPaciente').DataTable({});
}
//funcion para cargar el registro de lo spacientes
function cargarPaciente() {
    $('#cPaciente').html('');//limpiar la tabla
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/paciente/all",
        success: function (response) {
            response.forEach(i => {
                $('#cPaciente').append(
                    '<tr>' +
                    '<td>' + i.idPaciente + '</td>' +
                    '<td>' + i.nombre + '</td>' +
                    '<td>' + i.direccion + '</td>' +
                    '<td>' +
                    "<button onclick='mostrarRegistro(" + i.idPaciente + ");' type='button' class='btn btn-outline-dark ml-4 mt-1' data-toggle='modal' data-target='#mUpdate'>Editar</button>" +
                    "<button onclick='idPaciente(" + i.idPaciente + ");' type='button' class='btn btn-outline-danger ml-4 mt-1' data-toggle='modal' data-target='#mDelete'>Eliminar</button>" +
                    '</td>' +
                    '</tr>');
            });
        }
    });
}
//funcion para un mensaje de error
function errorPeticion(response) {
    alert(response.Mensaje);
}
//funcion para agregar un nuevo pacientes
function agregarPaciente() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/paciente/save",
        data: {
            nombre: $('#nombre').val(),
            direccion: $('#direccion').val()
        },
        success: function (response) {
            cargarPaciente();
            guardado();
            $('#nombre').val(null),
            $('#direccion').val(null)
        },
        error: errorPeticion
    });
}
//funcion para eliminar
function eliminarPaciente(id) {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/paciente/delete/" + id,
        success: function (response) {
            alert(response.Mensaje);
            cargarPaciente();
        },
        error: errorPeticion
    });
}
//funcioon para mostrar el registro dentro del modal
function mostrarRegistro(idPaciente) {
    $.ajax({
        url: "http://localhost:8080/paciente/gPaciente/" + idPaciente,
        method: "GET",
        success: function (response) {
            $("#idP").val(response.idPaciente);
            $("#nombre2").val(response.nombre);
            $("#direccion2").val(response.direccion);
        },
        error: errorPeticion
    });
}
//funcion para actulizar
function actualizarPaciente() {
    var id = $('#idP').val();
    $.ajax({
        url: "http://localhost:8080/paciente/update/" + id,
        method: "GET",
        data: {
            id: id,
            nombre: $("#nombre2").val(),
            direccion: $("#direccion2").val()
        },
        success: function (response) {
            alert(response.Mensaje);
            cargarPaciente();
        },
        error: errorPeticion
    });
}
//modals para las validaciones
function guardado() {
    Swal.fire(
        'Guardado',
        'El registro se guardo!',
        'success'
    )
}